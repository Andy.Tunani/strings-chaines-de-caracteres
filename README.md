Strings (cours)



* **_Syntax_**

        let str = ' Mon text 1';
        str = "Mon text 2"; 
        str= `Mon text 3`;

* **_Échappé les caractères_**

*utiliser un "\" Backslah

~~str = 'C'est';~~

        str = 'C\'est';  
        str= "C'est \"Mardi\"";
*saut de ligne avec "\n", peut rajouter plusieurs lignes

*rajouter un saut d'espace avec "\r"


        str = "\rMon text avec \nsaut de ligne"
console.log(str);

* **_Concaténation_**

*l'ajout de 2 chaines de caractère entre elles 



        let str2 = "Ma seconde chaine de caractère";
        str = "Mon text 2"; 
        str= `Mon text 3`;
console.log(str + str2 + " \n\nText3 ")

*se documenter pour manipuler les strings, en cherchant javascrip.string


___
**Connaitre la longueur d'une string**

* **_String : longueur_**

*compte combien de caractères

*tester et comparer grâce a la propriété "length"

        const str = "Mon text"

console.log(str.length, str.length < 3);






